package main

import (
	"flag"
	"path/filepath"
	"time"

	logs "github.com/Sirupsen/logrus"
	"github.com/golang/glog"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	informers "k8s.io/cosmosdb-controller/pkg/client/informers/externalversions"
	"k8s.io/cosmosdb-controller/pkg/signals"
	//"k8s.io/client-go/rest"

	examplecomclientset "k8s.io/cosmosdb-controller/pkg/client/clientset/versioned"
)

var (
	masterURL  string
	kubeconfig string
)

func kubeClient() (kubernetes.Interface, examplecomclientset.Interface) {
	stopCh := signals.SetupSignalHandler()

	//in-cluster
	// flag.StringVar(&kubeconfig, "kubeconfig", "", "Path to a kubeconfig. Only required if out-of-cluster.")
	// flag.StringVar(&masterURL, "master", "", "The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster.")
	// flag.Parse()

	// config, err := rest.InClusterConfig()
	// if err != nil {
	// 	panic(err.Error())
	// }

	//out-cluster
	home := homedir.HomeDir()
	flag.StringVar(&kubeconfig, "kubeconfig", filepath.Join(home, ".kube", "config"), "Path to a kubeconfig. Only required if out-of-cluster.")
	flag.StringVar(&masterURL, "master", "https://k8s-jermainea-master.northeurope.cloudapp.azure.com", "The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster.")
	flag.Parse()

	//use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags(masterURL, kubeconfig)
	if err != nil {
		glog.Fatalf("Error building kubeconfig: %s", err.Error())
	}

	// create the clientset
	cosmosdbClient, err := examplecomclientset.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	kubeClient, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(kubeClient, time.Second*30)
	cosmosdbInformerFactory := informers.NewSharedInformerFactory(cosmosdbClient, time.Second*30)

	controller := NewController(kubeClient, cosmosdbClient, kubeInformerFactory, cosmosdbInformerFactory)

	go kubeInformerFactory.Start(stopCh)
	go cosmosdbInformerFactory.Start(stopCh)

	if err = controller.Run(2, stopCh); err != nil {
		glog.Fatalf("Error running controller: %s", err.Error())
	}

	logs.Info("Successfully constructed k8s client")
	return kubeClient, cosmosdbClient
}

func main() {
	kubeClient()
}
