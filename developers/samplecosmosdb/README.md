# Helm deployment chart for sampleservicebus

## Prerequisites
1. Helm

### Creating a helm Chart
Please refer to sampleservicebus readme for more info about using this as a starter pack. You will also need access to a Kubernetes cluster.

```sh
# Deploy asb-controller image to cluster
$ helm install "./manifests/helm/sampleservicebus"

# check asb created through the custom resource
$ kubectl get asb --namespace=<namespace>
```

## Configuration

The following tables lists the configurable parameters of the sampleservicebus Chart and their values.

| Parameter | Description | Default |
|---------- | ----------- | ------- |
| `namespace` |  Namespace to create this resource | No default. This is mandatory |
| `resourceGroupName` | Resource group name in Azure. Refer keyvault | No default. This is mandatory |
| `namespaceName` | Namespace name. Refer keyvault | No default. This is mandatory |
| `authRuleName` | Auth Rule name. Refer keyvault | No default. This is mandatory |
| `queueName` | Queue name. Refer keyvault | No default. This is mandatory |
| `topicName` | Topic name. Refer keyvault | No default. This is mandatory |
| `subscriptionName` | Subscription name. Refer keyvault | No default. This is mandatory |
| `location` | Azure location for the database | Default to SQL logical server location |

