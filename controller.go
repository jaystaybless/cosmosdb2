/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"time"

	"github.com/Azure/azure-sdk-for-go/arm/keyvault"
	"github.com/Azure/azure-sdk-for-go/arm/resources/resources"
	logs "github.com/Sirupsen/logrus"

	"github.com/golang/glog"
	//_ "github.com/libcontroller"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/azure-sdk-for-go-samples/helpers"
	kubeinformers "k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
	clientset "k8s.io/cosmosdb-controller/pkg/client/clientset/versioned"
	asbscheme "k8s.io/cosmosdb-controller/pkg/client/clientset/versioned/scheme"
	informers "k8s.io/cosmosdb-controller/pkg/client/informers/externalversions"
	listers "k8s.io/cosmosdb-controller/pkg/client/listers/cosmosdbcontroller/v1"

	"github.com/Azure/azure-sdk-for-go/services/cosmos-db/mgmt/2015-04-08/documentdb"
)

const controllerAgentName = "cosmosdb-controller"

var (
	groupsClient   resources.GroupsClient
	keyvaultClient keyvault.VaultsClient

	databaseClient documentdb.DatabaseAccountsClient

//cosmosClient servicebus.NamespacesClient

)

const (
	// SuccessSynced is used as part of the Event 'reason' when a Foo is synced
	SuccessSynced = "Synced"
	// ErrResourceExists is used as part of the Event 'reason' when a Foo fails
	// to sync due to a Deployment of the same name already existing.
	ErrResourceExists = "ErrResourceExists"

	// MessageResourceExists is the message used for Events when a resource
	// fails to sync due to a Deployment already existing
	MessageResourceExists = "Resource %q already exists and is not managed by CosmosDatabase"
	// MessageResourceSynced is the message used for an Event fired when a Foo
	// is synced successfully
	MessageResourceSynced = "CosmosDatabase synced successfully"
)

// Controller is the controller implementation for CosmosDatabase resources
type Controller struct {
	logger *logs.Entry
	// kubeclientset is a standard kubernetes clientset
	kubeclientset kubernetes.Interface
	// sampleclientset is a clientset for our own API group
	cosmosclientset clientset.Interface

	cosmosLister listers.CosmosDatabaseLister
	cosmosSynced cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder
}

// NewController returns a new CosmosDatabase controller
func NewController(
	kubeclientset kubernetes.Interface,
	cosmosclientset clientset.Interface,
	kubeInformerFactory kubeinformers.SharedInformerFactory,
	cosmosInformerFactory informers.SharedInformerFactory) *Controller {

	// obtain references to shared index informers for the Deployment and Foo
	// types.
	cosmosInformer := cosmosInformerFactory.Cosmosdbcontroller().V1().CosmosDatabases()

	// Create event broadcaster
	// Add asb-controller types to the default Kubernetes Scheme so Events can be
	// logged for asb-controller types.
	asbscheme.AddToScheme(scheme.Scheme)
	glog.V(4).Info("Creating event broadcaster")
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartLogging(glog.Infof)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: kubeclientset.CoreV1().Events("")})
	recorder := eventBroadcaster.NewRecorder(scheme.Scheme, corev1.EventSource{Component: controllerAgentName})

	controller := &Controller{
		kubeclientset:   kubeclientset,
		cosmosclientset: cosmosclientset,
		cosmosLister:    cosmosInformer.Lister(),
		cosmosSynced:    cosmosInformer.Informer().HasSynced,
		workqueue:       workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "Foos"),
		recorder:        recorder,
	}

	glog.Info("Setting up event handlers")
	// Set up an event handler for when Foo resources change
	cosmosInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: controller.enqueueCosmosDatabase,
		UpdateFunc: func(old, new interface{}) {
			controller.enqueueCosmosDatabase(new)
		},
		DeleteFunc: controller.enqueueCosmosDatabase,
	})
	// Set up an event handler for when Deployment resources change. This
	// handler will lookup the owner of the given Deployment, and if it is
	// owned by a Foo resource will enqueue that Foo resource for
	// processing. This way, we don't need to implement custom logic for
	// handling Deployment resources. More info on this pattern:
	// https://github.com/kubernetes/community/blob/8cafef897a22026d42f5e5bb3f104febe7e29830/contributors/devel/controllers.md

	return controller
}

// Run will set up the event handlers for types we are interested in, as well
// as syncing informer caches and starting workers. It will block until stopCh
// is closed, at which point it will shutdown the workqueue and wait for
// workers to finish processing their current work items.
func (c *Controller) Run(threadiness int, stopCh <-chan struct{}) error {
	defer runtime.HandleCrash()
	defer c.workqueue.ShutDown()

	logs.Info("Controller.Run: initiating")

	// Start the informer factories to begin populating the informer caches
	logs.Info("Starting CosmosDatabase controller")

	// run the informer to start listing and watching resources

	// Wait for the caches to be synced before starting workers
	logs.Info("Waiting for informer caches to sync")
	if ok := cache.WaitForCacheSync(stopCh, c.cosmosSynced); !ok {
		return fmt.Errorf("failed to wait for caches to sync")
	}

	glog.Info("Starting workers")
	// Launch two workers to process CosmosDatabase resources
	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	logs.Info("Started workers")
	<-stopCh
	logs.Info("Shutting down workers")

	return nil
}

// runWorker is a long-running function that will continually call the
// processNextWorkItem function in order to read and process a message on the
// workqueue.
func (c *Controller) runWorker() {
	logs.Info("Controller.runWorker: starting")

	// invoke processNextItem to fetch and consume the next change
	// to a watched or listed resource
	for c.processNextWorkItem() {
		logs.Info("Controller.runWorker: processing next item")
	}

	logs.Info("Controller.runWorker: completed")
}

// processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	logs.Info("Controller.processNextItem: start")

	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			runtime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		// Run the syncHandler, passing it the namespace/name string of the
		// Foo resource to be synced.
		if err := c.syncHandler(key); err != nil {
			return fmt.Errorf("error syncing '%s': %s", key, err.Error())
		}
		// uncomment this later!!!!!
		// if err := listResource(key); err != nil {
		// 	return fmt.Errorf("error syncing '%s': %s", key, err.Error())
		// }
		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		glog.Infof("Successfully synced '%s'", key)
		return nil
	}(obj)

	if err != nil {
		runtime.HandleError(err)
		return true
	}

	return true
}

// syncHandler compares the actual state with the desired, and attempts to
// converge the two. It then updates the Status block of the Azure Service Bus resource
// with the current status of the resource.
func (c *Controller) syncHandler(key string) error {
	logs.Info("Controller.syncHandler: start")
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		runtime.HandleError(fmt.Errorf("invalid resource key: %s", key))
		return nil
	}

	obj, err := c.cosmosLister.CosmosDatabases(namespace).Get(name)

	glog.Infof("Get the object if it exist: %+v", obj)

	glog.Infof("Get the object if it exist: %+v", err)

	if err != nil {
		// 	// Handle deletion of resources here - As the resources don't exist anymore in Kubernetes, we use the
		// 	// object passed in the message.

		fmt.Printf("Mock deletion of Cosmos Database %s\n")

		// _, err = tagDatabaseForDeletion(*obj)
		// if err != nil {
		// 	helpers.PrintAndLog(fmt.Sprintf("ERROR - cannot tag database account for deletion: %v", err))
		// }
		// fmt.Printf("---database account tagged for deletion---\n")

	} else {

		logs.Info("Sync/Add/Update of azure service bus ", obj.GetName())

		// _, err := createOrUpdateResourceGroup(*obj)
		// if err != nil {
		// 	return fmt.Errorf("Create failed: %v - +%v", err, obj)
		// }

		// _, err = createOrUpdateDatabase(*obj)
		// if err != nil {
		// 	helpers.PrintAndLog(fmt.Sprintf("ERROR - cannot create database account: %v", err))
		// }
		// fmt.Printf("---database account created---\n")

		// _, err = createOrUpdateKeyVault(*obj)
		// if err != nil {
		// 	helpers.PrintAndLog(fmt.Sprintf("ERROR - cannot create vault in KeyVault: %v", err))
		// }
		// fmt.Printf("---vault created in keyvault---\n")

		_, err = listKeys(*obj)
		if err != nil {
			helpers.PrintAndLog(fmt.Sprintf("ERROR - cannot list keys for database account: %v", err))
		}
		fmt.Printf("---keys listed---\n")

		// _, err = createOrUpdateKeyVaultKeys(*obj)
		// if err != nil {
		// 	helpers.PrintAndLog(fmt.Sprintf("ERROR - cannot create keys in KeyVault: %v", err))
		// }
		//fmt.Printf("---keys created in keyvault---\n")

		//vaultName := "cosmosdb-test-3"
		//CreateKeyBundle(vaultName)
		// _, err = listKeys(*obj)
		// if err != nil {
		// 	helpers.PrintAndLog(fmt.Sprintf("ERROR - cannot create keys in KeyVault: %v", err))
		// }

	}
	return nil

}

// enqueueCosmosDatabase takes a CosmosDatabase resource and converts it into a namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than CosmosDatabase.
func (c *Controller) enqueueCosmosDatabase(obj interface{}) {
	var key string
	var err error
	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		runtime.HandleError(err)
		return
	}
	c.workqueue.AddRateLimited(key)
}

// handleObject will take any resource implementing metav1.Object and attempt
// to find the CosmosDatabase resource that 'owns' it. It does this by looking at the
// objects metadata.ownerReferences field for an appropriate OwnerReference.
// It then enqueues that CosmosDatabase resource to be processed. If the object does not
// have an appropriate OwnerReference, it will simply be skipped.
func (c *Controller) handleObject(obj interface{}) {
	var object metav1.Object
	var ok bool
	if object, ok = obj.(metav1.Object); !ok {
		tombstone, ok := obj.(cache.DeletedFinalStateUnknown)
		if !ok {
			runtime.HandleError(fmt.Errorf("error decoding object, invalid type"))
			return
		}
		object, ok = tombstone.Obj.(metav1.Object)
		if !ok {
			runtime.HandleError(fmt.Errorf("error decoding object tombstone, invalid type"))
			return
		}
		glog.V(4).Infof("Recovered deleted object '%s' from tombstone", object.GetName())
	}
	glog.V(4).Infof("Processing object: %s", object.GetName())
	if ownerRef := metav1.GetControllerOf(object); ownerRef != nil {
		// If this object is not owned by a CosmosDatabase, we should not do anything more
		// with it.
		if ownerRef.Kind != "CosmosDatabase" {
			return
		}

		obj, err := c.cosmosLister.CosmosDatabases(object.GetNamespace()).Get(ownerRef.Name)
		if err != nil {
			glog.V(4).Infof("ignoring orphaned object '%s' of CosmosDatabase '%s'", object.GetSelfLink(), ownerRef.Name)
			return
		}

		c.enqueueCosmosDatabase(obj)
		return
	}
}
