package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	// "github.com/Azure/azure-sdk-for-go/arm/keyvault"
	"github.com/Azure/azure-sdk-for-go/arm/keyvault"
	"github.com/Azure/azure-sdk-for-go/arm/resources/resources"
	"github.com/Azure/azure-sdk-for-go/services/cosmos-db/mgmt/2015-04-08/documentdb"
	"github.com/Azure/azure-sdk-for-go/services/cosmos-db/mongodb"
	"github.com/Azure/go-autorest/autorest"
	"github.com/Azure/go-autorest/autorest/azure/auth"
	"github.com/Azure/go-autorest/autorest/to"
	"github.com/golang/glog"
	"github.com/satori/uuid"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/azure-sdk-for-go-samples/helpers"
	"k8s.io/azure-sdk-for-go-samples/iam"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	cosmosv1 "k8s.io/cosmosdb-controller/pkg/apis/cosmosdbcontroller/v1"
	"k8s.io/go-autorest/autorest/azure"

	kvault "github.com/Azure/azure-sdk-for-go/services/keyvault/2016-10-01/keyvault"
	// metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	ctx = context.Background()
)

func NewAuthorizerFromEnvironment() (autorest.Authorizer, error) {
	envName := os.Getenv("AZURE_ENVIRONMENT")
	var env azure.Environment
	var err error

	if envName == "" {
		env = azure.PublicCloud
	} else {
		env, err = azure.EnvironmentFromName(envName)
		if err != nil {
			return nil, err
		}
	}

	resource := os.Getenv("AZURE_KEYVAULT_RESOURCE")
	if resource == "" {
		resource = strings.TrimSuffix(env.KeyVaultEndpoint, "/")
	}

	return auth.NewAuthorizerFromEnvironmentWithResource(resource)
}

func getGroupsClient() resources.GroupsClient {
	groupsClient := resources.NewGroupsClient(helpers.SubscriptionID())
	auth, _ := iam.GetResourceManagementToken(iam.AuthGrantType())
	groupsClient.Authorizer = autorest.NewBearerAuthorizer(auth) //auth
	groupsClient.AddToUserAgent(helpers.UserAgent())
	return groupsClient
}

func createOrUpdateResourceGroup(cosmosDB cosmosv1.CosmosDatabase) (rsg resources.Group, err error) {
	resourceGroupName := cosmosDB.Spec.ResourceGroupName
	location := cosmosDB.Spec.Location
	tags := make(map[string]*string)
	if cosmosDB.Spec.Tags != nil {
		val := make([]string, len(cosmosDB.Spec.Tags))
		idx := 0
		for k, v := range cosmosDB.Spec.Tags {
			val[idx] = v
			tags[k] = &val[idx]
			idx++
		}
	}

	if resourceGroupName == "" {
		//runtime.HandleError(fmt.Errorf("%s: deployment name must be specified for Resource Group", resourceGroupName))
		return rsg, fmt.Errorf("(resourceGroupName) name must be specified for Resource Group: %v", resourceGroupName)
	}
	if location == "" {
		//runtime.HandleError(fmt.Errorf("%s: location name must be specified for Resource Group", location))
		return rsg, fmt.Errorf("(location) name must be specified for Resource Group: %v", location)

	}

	fmt.Printf("Creating resource group '%s'...\n", resourceGroupName)

	groupsClient := getGroupsClient()

	groupParams := resources.Group{
		Location: to.StringPtr(location),
		Tags:     &tags,
	}

	resourceGroup, err := groupsClient.CreateOrUpdate(
		resourceGroupName, groupParams)
	fmt.Printf("Created Resource Group '%s'...\n", resourceGroup)

	if err != nil {
		return rsg, fmt.Errorf("Resource Group rule creation failed: %v", err)
	}

	return rsg, err
}

func getVaultsClient() keyvault.VaultsClient {
	vaultsClient := keyvault.NewVaultsClient(helpers.SubscriptionID())
	auth, _ := iam.GetResourceManagementToken(iam.AuthGrantType())
	//fmt.Printf("-- 22auth22 -- : '%s'...\n", auth)
	vaultsClient.Authorizer = autorest.NewBearerAuthorizer(auth) //auth
	vaultsClient.AddToUserAgent(helpers.UserAgent())
	return vaultsClient
}

func createOrUpdateKeyVault(cosmosDB cosmosv1.CosmosDatabase) (keyName keyvault.Vault, err error) {
	applicationId := os.Getenv("AZ_CLIENT_ID")
	tenantId := uuid.FromStringOrNil(os.Getenv("AZ_TENANT_ID"))

	resourceGroupName := cosmosDB.Spec.ResourceGroupName
	location := cosmosDB.Spec.Location
	vaultName := cosmosDB.Spec.ID
	tags := make(map[string]*string)
	if cosmosDB.Spec.Tags != nil {
		val := make([]string, len(cosmosDB.Spec.Tags))
		idx := 0
		for k, v := range cosmosDB.Spec.Tags {
			val[idx] = v
			tags[k] = &val[idx]
			idx++
		}
	}

	vaultsClient := getVaultsClient()

	keyvaultParams := keyvault.VaultCreateOrUpdateParameters{
		Location: to.StringPtr(location),
		Tags:     &tags,
		Properties: &keyvault.VaultProperties{
			Sku: &keyvault.Sku{
				Name:   keyvault.SkuName("Standard"),
				Family: to.StringPtr("A"),
			},
			AccessPolicies: &[]keyvault.AccessPolicyEntry{
				{
					TenantID: &tenantId,
					ObjectID: &applicationId,
					Permissions: &keyvault.Permissions{
						Secrets: &[]keyvault.SecretPermissions{
							keyvault.SecretPermissionsAll,
						},
					},
				},
			},
			TenantID: &tenantId,
		},
	}

	fmt.Printf("Creating Vault: '%s'...\n", vaultName)

	vault, err := vaultsClient.CreateOrUpdate(resourceGroupName, vaultName, keyvaultParams)
	fmt.Printf("vault info:'%s'...\n", vault)
	if err != nil {
		return keyName, fmt.Errorf("Vault creation failed: %v", err)
	}

	return keyName, err
}

func getDatabaseAccountClient() documentdb.DatabaseAccountsClient {
	dbAccountClient := documentdb.NewDatabaseAccountsClient(helpers.SubscriptionID())
	auth, _ := iam.GetResourceManagementToken(iam.AuthGrantType())
	dbAccountClient.Authorizer = autorest.NewBearerAuthorizer(auth) //auth
	dbAccountClient.AddToUserAgent(helpers.UserAgent())
	return dbAccountClient
}

// CreateDatabaseAccount creates or updates an Azure Cosmos DB database account.
func createOrUpdateDatabase(cosmosDB cosmosv1.CosmosDatabase) (dba documentdb.DatabaseAccount, err error) {
	//databaseAccountOfferType := cosmosDB.Spec.Properties
	databaseAccountOfferType := "Standard"
	resourceGroupName := cosmosDB.Spec.ResourceGroupName
	accountName := cosmosDB.Spec.ID
	kind := cosmosDB.Spec.Kind
	// id := cosmosDB.Spec.ID
	location := cosmosDB.Spec.Location

	tags := make(map[string]*string)
	if cosmosDB.Spec.Tags != nil {
		val := make([]string, len(cosmosDB.Spec.Tags))
		idx := 0
		for k, v := range cosmosDB.Spec.Tags {
			val[idx] = v
			tags[k] = &val[idx]
			idx++
		}
	}

	fmt.Printf("Creating Database Account '%s'...\n", accountName)

	dbAccountClient := getDatabaseAccountClient()

	databaseParams := documentdb.DatabaseAccountCreateUpdateParameters{
		Location: to.StringPtr(location),
		Tags:     tags,
		Kind:     documentdb.DatabaseAccountKind(kind), //documentdb.GlobalDocumentDB,
		DatabaseAccountCreateUpdateProperties: &documentdb.DatabaseAccountCreateUpdateProperties{
			DatabaseAccountOfferType: to.StringPtr(databaseAccountOfferType),
			Locations: &[]documentdb.Location{
				{
					FailoverPriority: to.Int32Ptr(0),
					LocationName:     to.StringPtr(location),
				},
			},
		},
	}

	future, err := dbAccountClient.CreateOrUpdate(ctx, resourceGroupName, accountName, databaseParams)
	if err != nil {
		return dba, fmt.Errorf("%v", err)
	}

	err = future.WaitForCompletion(ctx, dbAccountClient.Client)
	if err != nil {
		return dba, fmt.Errorf("cannot get the database account create or update future response: %v", err)
	}

	return future.Result(dbAccountClient)
}

func tagDatabaseForDeletion(cosmosDB cosmosv1.CosmosDatabase) (dba documentdb.DatabaseAccount, err error) {
	fmt.Printf("tagDatabaseForDeletion() FUNCTION has been called...\n")

	databaseAccountOfferType := cosmosDB.Spec.Properties.DatabaseAccountOfferType
	resourceGroupName := cosmosDB.Spec.ResourceGroupName
	accountName := cosmosDB.Spec.Kind
	kind := cosmosDB.Spec.Kind
	// id := cosmosDB.Spec.ID
	location := cosmosDB.Spec.Location

	namespace := cosmosDB.Namespace
	tagName := "DELETE" + "-" + accountName + "-" + location + "-" + namespace + "-ns-rg-DELETE"
	tags := map[string]*string{
		"tag-name": to.StringPtr(tagName),
	}

	// tags := make(map[string]*string)
	// if cosmosDB.Spec.Tags != nil {
	// 	val := make([]string, len(cosmosDB.Spec.Tags))
	// 	idx := 0
	// 	for k, v := range cosmosDB.Spec.Tags {
	// 		val[idx] = v
	// 		tags[k] = &val[idx]
	// 		idx++
	// 	}
	// }

	fmt.Printf("Tagging Database Account for Deletion'%s'...\n", accountName)

	dbAccountClient := getDatabaseAccountClient()

	databaseParams := documentdb.DatabaseAccountCreateUpdateParameters{
		Location: to.StringPtr(location),
		Tags:     tags,
		Kind:     documentdb.DatabaseAccountKind(kind), //documentdb.GlobalDocumentDB,
		DatabaseAccountCreateUpdateProperties: &documentdb.DatabaseAccountCreateUpdateProperties{
			DatabaseAccountOfferType: databaseAccountOfferType,
			Locations: &[]documentdb.Location{
				{
					FailoverPriority: to.Int32Ptr(0),
					LocationName:     to.StringPtr(location),
				},
			},
		},
	}

	future, err := dbAccountClient.CreateOrUpdate(ctx, resourceGroupName, accountName, databaseParams)
	if err != nil {
		return dba, fmt.Errorf("%v", err)
	}

	err = future.WaitForCompletion(ctx, dbAccountClient.Client)
	if err != nil {
		return dba, fmt.Errorf("cannot get the database account create or update future response: %v", err)
	}

	return future.Result(dbAccountClient)
}

func getKeysClient() kvault.BaseClient {
	keyClient := kvault.New()
	auth, _ := iam.GetKeyvaultToken(iam.AuthGrantType())
	keyClient.Authorizer = auth
	keyClient.AddToUserAgent(helpers.UserAgent())
	return keyClient
}

// ListKeys gets the keys for a Azure Cosmos DB database account.
func listKeys(cosmosDB cosmosv1.CosmosDatabase) (documentdb.DatabaseAccountListKeysResult, error) {
	resourceGroupName := cosmosDB.Spec.ResourceGroupName
	accountName := cosmosDB.Spec.ID
	namespace := cosmosDB.Namespace
	location := cosmosDB.Spec.Location
	environment := os.Getenv("ENVIRONMENT")
	tagName := "pltsrv-" + environment + "-" + location + "-" + namespace + "-ns-rg"
	tags := make(map[string]*string)
	if cosmosDB.Spec.Tags != nil {
		val := make([]string, len(cosmosDB.Spec.Tags))
		idx := 0
		for k, v := range cosmosDB.Spec.Tags {
			val[idx] = v
			tags[k] = &val[idx]
			idx++
		}
	}
	dbAccountClient := getDatabaseAccountClient()

	fmt.Printf("resourceGroupName '%s'...\n", resourceGroupName)
	fmt.Printf("Listing Keys for Database Account '%s'...\n", accountName)

	keys, err := dbAccountClient.ListKeys(ctx, resourceGroupName, accountName)
	if err != nil {
		fmt.Printf("\tAn error has occured whilst listing key's from Cosmos DB. ERROR: %s\n", err)
	}
	connectionString := "AccountEndpoint=https://" + accountName + ".documents.azure.com:443/;AccountKey="
	uriString := "https://" + accountName + ".documents.azure.com:443/"

	//ReadWrite Keys
	primaryReadWriteMasterKey := *keys.PrimaryMasterKey
	primaryReadWriteConnectionString := connectionString + primaryReadWriteMasterKey + ";"
	secondaryReadWriteMasterKey := *keys.SecondaryMasterKey
	secondaryReadWriteConnectionString := connectionString + secondaryReadWriteMasterKey + ";"

	//Create Secrets in k8s
	//go storeSecret(primaryReadWriteConnectionString, secondaryReadWriteConnectionString, primaryReadWriteMasterKey, secondaryReadWriteMasterKey, namespace, accountName)

	// (*primaryReadWriteConnectionString, *secondaryReadWriteConnectionString, *primaryReadWriteMasterKey, *secondaryReadWriteMasterKey, *namespace, *accountName)

	//create secrets in kubernetes cluster
	kubeClient := getClient()

	secretData := map[string]string{
		"primaryReadWriteConnectionString":   primaryReadWriteConnectionString,
		"secondaryReadWriteConnectionString": secondaryReadWriteConnectionString,
		"primaryReadWriteMasterKey":          primaryReadWriteMasterKey,
		"secondaryReadWriteMasterKey":        secondaryReadWriteMasterKey,
	}

	secretConfig := &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      accountName,
			Namespace: namespace,
			Labels: map[string]string{
				"name": tagName,
			},
			//Labels:    *tags,
		},
		StringData: secretData,
	}
	_, err = kubeClient.CoreV1().Secrets(namespace).Get(accountName, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		fmt.Printf("Secret %s in namespace %s not found\n", accountName, namespace)
		_, err = kubeClient.CoreV1().Secrets(namespace).Create(secretConfig)
		if err != nil {
			//fmt.Printf("Cannot update create %s\n", sc)
			glog.Fatalf("Cannot create secret %s", secretConfig)
		}
	}
	if statusError, isStatus := err.(*errors.StatusError); isStatus {
		fmt.Printf("Error getting secret %s in namespace %s: %v\n",
			accountName, namespace, statusError.ErrStatus.Message)
	}
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Printf("Found secret %s in namespace %s\n", accountName, namespace)
		fmt.Printf("Secret %s in namespace %s already exists\n", accountName, namespace)
		fmt.Printf("UPDATING Secret %s in namespace %s\n", secretConfig.Name, namespace)
		_, err = kubeClient.CoreV1().Secrets(namespace).Update(secretConfig)
		if err != nil {
			glog.Fatalf("Cannot update secret %s", secretConfig)
		}
	}

	//Creating Secrets In KeyVault
	fmt.Printf("------Creating Keys in KeyVault...\n")
	vaultName := cosmosDB.Spec.ID

	//kvault = kvault.KeyBundle()
	vaultsClient := getVaultsClient()
	vault, err := vaultsClient.Get(resourceGroupName, vaultName)
	if err != nil {
		fmt.Printf("Error occured whilst retrieving keyvault:'%s'...\n", vaultName)
	}
	fmt.Printf("vault info:'%s'...\n", vault)
	vaultURL := *vault.Properties.VaultURI
	fmt.Printf("Vault URL:  '%s'...\n", vaultURL)

	keyClient := getKeysClient()

	//secretName := primaryReadWriteConnectionString
	secretName := "tempSecret"

	secretParams := kvault.SecretSetParameters{
		Value: &secretName,
		Tags:  tags,
		SecretAttributes: &kvault.SecretAttributes{
			Enabled: to.BoolPtr(true),
		},
	}

	_, err = keyClient.SetSecret(ctx, vaultURL, secretName, secretParams)
	if err != nil {
		fmt.Printf("---------Vault Secret creation failed: %v\n", err)
	}

	//Test connection to MongoDB
	host := accountName + ".documents.azure.com"
	session, err := mongodb.NewMongoDBClientWithCredentials(accountName, primaryReadWriteMasterKey, host)
	if err != nil {
		helpers.PrintAndLog(fmt.Sprintf("cannot get mongoDB session: %v", err))
	}

	fmt.Printf("\t Mongodb Host, Mongodb Session, URI, Primary and Secondary Connection Strings -- Mongodb Host: %s, Mongodb Session: %s ,uriString: %s, primaryReadWriteConnectionString: %s, secondaryReadWriteConnectionString: %s\n", host, session, uriString, primaryReadWriteConnectionString, secondaryReadWriteConnectionString)

	return dbAccountClient.ListKeys(ctx, resourceGroupName, accountName)
	//return nil
}

//get a clientset with in-cluster config.
func getClient() *kubernetes.Clientset {
	// config, err := rest.InClusterConfig()
	fmt.Printf("HELLO from getClient")
	config, err := clientcmd.BuildConfigFromFlags(masterURL, kubeconfig)
	if err != nil {
		glog.Fatal(err)
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		glog.Fatal(err)
	}
	return clientset
}

// func storeSecret(primaryReadWriteConnectionString, secondaryReadWriteConnectionString, primaryReadWriteMasterKey, secondaryReadWriteMasterKey, namespace, accountName string) (err error) {

// 	cosmosDB := *cosmosv1.CosmosDatabase
// 	resourceGroupName := cosmosDB.Spec.ResourceGroupName
// 	fmt.Printf("resourceGroupName from storeSecret'%s'...\n", resourceGroupName)

// 	accountName = cosmosDB.Spec.ID
// 	namespace = cosmosDB.Namespace
// 	location := cosmosDB.Spec.Location
// 	environment := os.Getenv("ENVIRONMENT")
// 	tagName := "pltsrv-" + environment + "-" + location + "-" + namespace + "-ns-rg"
// 	fmt.Printf("namespace from storeSecret'%s'...\n", namespace)
// 	// tags := make(map[string]*string)
// 	// if cosmosDB.Spec.Tags != nil {
// 	// 	val := make([]string, len(cosmosDB.Spec.Tags))
// 	// 	idx := 0
// 	// 	for k, v := range cosmosDB.Spec.Tags {
// 	// 		val[idx] = v
// 	// 		tags[k] = &val[idx]
// 	// 		idx++
// 	// 	}
// 	// }

// 	//create secrets in kubernetes cluster
// 	kubeClient := getClient()

// 	secretData := map[string]string{
// 		"primaryReadWriteConnectionString":   primaryReadWriteConnectionString,
// 		"secondaryReadWriteConnectionString": secondaryReadWriteConnectionString,
// 		"primaryReadWriteMasterKey":          primaryReadWriteMasterKey,
// 		"secondaryReadWriteMasterKey":        secondaryReadWriteMasterKey,
// 	}

// 	secretConfig := &v1.Secret{
// 		ObjectMeta: metav1.ObjectMeta{
// 			Name:      accountName,
// 			Namespace: namespace,
// 			Labels: map[string]string{
// 				"name": tagName,
// 			},
// 			//Labels:    *tags,
// 		},
// 		StringData: secretData,
// 	}
// 	_, err = kubeClient.CoreV1().Secrets(namespace).Get(accountName, metav1.GetOptions{})
// 	if errors.IsNotFound(err) {
// 		fmt.Printf("Secret %s in namespace %s not found\n", accountName, namespace)
// 		_, err = kubeClient.CoreV1().Secrets(namespace).Create(secretConfig)
// 		if err != nil {
// 			//fmt.Printf("Cannot update create %s\n", sc)
// 			glog.Fatalf("Cannot create secret %s", secretConfig)
// 		}
// 	}
// 	if statusError, isStatus := err.(*errors.StatusError); isStatus {
// 		fmt.Printf("Error getting secret %s in namespace %s: %v\n",
// 			accountName, namespace, statusError.ErrStatus.Message)
// 	}
// 	if err != nil {
// 		panic(err.Error())
// 	} else {
// 		fmt.Printf("Found secret %s in namespace %s\n", accountName, namespace)
// 		fmt.Printf("Secret %s in namespace %s already exists\n", accountName, namespace)
// 		fmt.Printf("UPDATING Secret %s in namespace %s\n", secretConfig.Name, namespace)
// 		_, err = kubeClient.CoreV1().Secrets(namespace).Update(secretConfig)
// 		if err != nil {
// 			glog.Fatalf("Cannot update secret %s", secretConfig)
// 		}
// 	}

// 	return
// 	//return primaryReadWriteConnectionString, secondaryReadWriteConnectionString, primaryReadWriteMasterKey, secondaryReadWriteMasterKey, namespace, accountName, nil
// }

/// Handles Tagging
// namespace := cosmosDB.Namespace
// environment := os.Getenv("ENVIRONMENT")
// tagName := "pltsrv-" + environment + "-" + location + "-" + namespace + "-ns-rg"
// tags := map[string]*string{
// 	"tag-name": to.StringPtr(tagName),
// }
///
