# asb-controller (azure service bus controller)

## Overview
This repository implements a simple controller for watching AzureServiceBus resources as
defined with a CustomResourceDefinition (CRD).

This particular example demonstrates how to perform basic operations such as:

* How to register a new custom resource (custom resource type) of type `AzureServiceBus` using a CustomResourceDefinition.
* How to create/get/list instances of your new resource type `AzureServiceBus`.
* How to setup a controller on resource handling create/update/delete events.

A few short names have will be available after the CRD is created.
```
    kind: AzureServiceBus
    ...
    shortNames:
    - asb
```

## Installation
On a very high level, the installation involves the following steps:

### On cluster ###
1. Create a CRD resource on the cluster.

### On local ###
1. Ensure Azure environment variables exists. This will be the ARM_xxxx env vars mentioned on Kubernetes-platform readme, step 7. The Vault address "VAULT_ADDR" (url i.e https://orgname.department.net), Token "TOKEN" and environment "ENVIR" to be deployed to will need to be set.
2. Build controller container and push the image to our docker registry.
3. Source *setenv.sh*. This will copy the ARM env vars to AZURE_xxx to be consumed by the controller.
4. Install Helm Chart.0

## Dependencies
1. Go dependency manager, Dep.
On Mac, you can install it using brew
`brew install dep`

2. Docker

3. Helm

You will also need the credential for our docker registry.


## Required Fields

* apiVersion - The Kubernetes API version. See "Azure SQL Database" Third Party Resource.
* kind - The Kubernetes object type.
* metadata.name - Specifies the name of a server
* namespace - Namespace name of the target resource
* spec.resourceGroupName - The resource group name
* spec.namespaceName - Name of the namespace for azure resource (servicebus)
* spec.authRuleName - Name of the auth rule for azure resource (servicebus)
* spec.queueName - Name of the queue
* spec.topicName - Name of the topic
* spec.subscriptionName - Name of the subscription
* spec.location - Location of the resource

### Example Azure Service Bus Object

```
apiVersion: platform1.platform.mnscorp.net/v1
kind: AzureServiceBus
metadata:
  name: my-azureservicebus-3
  namespace: platform-services
spec: 
  location: northeurope
  resourceGroupName: pltsrv-jermainea-crd-test-3-eun-k8s-clstr-rg
  namespaceName: jermainea-crd-test-3
  authRuleName: jermainea-authrule-crd-test-3
  queueName  : jermainea-myqueue-crd-test-3
  topicName: jermainea-topic3-crd-test-3
  subscriptionName: jermainea-esub3-crd-test-3
```

## Run

The controller requires a few environment variables to be present:

#### AZURE_SUBSCRIPTION_ID

The subscription ID to use.

#### AZURE_CLIENT_ID

The tenant ID to use.

#### AZURE_CLIENT_SECRET

The client secret to use.

#### AZURE_TENANT_ID

The client ID to use.

#### VAULT_ADDRESS

The vault address to use.

#### VAULT_TOKEN

The vault token to use.

#### ENVIRONMENT

The envrionment to use.


The following command assumes you have a working kubeconfig (not required if operating in-cluster)
```
go run *.go -kubeconfig=$HOME/.kube/config
```


## Detailed Installation step

### On cluster ###
**Prerequisite**: Since the asb-controller uses `apps/v1`, the Kubernetes cluster version should be greater than 1.9.

```sh
# Deploy asb-controller image to cluster
$ helm install asb-controller "./manifests/helm/asb-controller"

# create a CustomResourceDefinition
$ kubectl create -f artifacts/examples/azure-service-bus-crd.yaml

# create a custom resource of type AzureServiceBus
$ kubectl create -f artifacts/examples/azure-service-bus-crd-example.yaml

# check asb created through the custom resource
$ kubectl get asb --namespace=<namespace>
```


### On local ###

```sh
# assumes you have a working kubeconfig, not required if operating in-cluster
$ go run *.go -kubeconfig=$HOME/.kube/config -masterURL=https://kubernetes.cluster.example.com

# create a CustomResourceDefinition
$ kubectl create -f artifacts/examples/azure-service-bus-crd.yaml

# create a custom resource of type AzureServiceBus
$ kubectl create -f artifacts/examples/azure-service-bus-crd-example.yaml

# check asb created through the custom resource
$ kubectl get asb --namespace=<namespace>

# check deployments created through the custom resource
$ kubectl get deployments
```

## Cleanup

You can clean up the created CustomResourceDefinition with:

    $ kubectl delete crd azureservicebuses.<dev|perf|prod>.platform.mnscorp.net


