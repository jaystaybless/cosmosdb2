// +k8s:deepcopy-gen=package,register

// Package v1 is the v1 version of the API.
// +groupName=cosmosdbcontroller.k8s.io
package v1
