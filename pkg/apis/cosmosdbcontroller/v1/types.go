package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +genclient:noStatus
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CosmosDatabase describes a CosmosDatabase.
type CosmosDatabase struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec CosmosDatabaseSpec `json:"spec"`
}

// CosmosDatabaseSpec is the spec for a CosmosDatabase resource
type CosmosDatabaseSpec struct {
	Location          string                     `json:"location"`
	ResourceGroupName string                     `json:"resourceGroupName"`
	Kind              string                     `json:"kind"`
	ID                string                     `json:"id"`
	Tags              map[string]string          `json:"tags,omitempty"`
	Properties        *DatabaseAccountProperties `json:"properties,omitempty"`
	//Encoding string `json:"encoding,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// CosmosDatabaseList is a list of CosmosDatabase resources
type CosmosDatabaseList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []CosmosDatabase `json:"items"`
}

type DatabaseAccountProperties struct {
	// ConsistencyPolicy - The consistency policy for the Cosmos DB account.
	//ConsistencyPolicy *ConsistencyPolicy `json:"consistencyPolicy,omitempty"`
	// Locations - An array that contains the georeplication locations enabled for the Cosmos DB account.
	//Locations                *[]Location `json:"locations,omitempty"`
	DatabaseAccountOfferType *string `json:"databaseAccountOfferType,omitempty"`
	// IPRangeFilter - Cosmos DB Firewall Support: This value specifies the set of IP addresses or IP address ranges in CIDR form to be included as the allowed list of client IPs for a given database account. IP addresses/ranges must be comma separated and must not contain any spaces.
	//IPRangeFilter *string `json:"ipRangeFilter,omitempty"`
	// IsVirtualNetworkFilterEnabled - Flag to indicate whether to enable/disable Virtual Network ACL rules.
	//IsVirtualNetworkFilterEnabled *bool `json:"isVirtualNetworkFilterEnabled,omitempty"`
	// EnableAutomaticFailover - Enables automatic failover of the write region in the rare event that the region is unavailable due to an outage. Automatic failover will result in a new write region for the account and is chosen based on the failover priorities configured for the account.
	//EnableAutomaticFailover *bool `json:"enableAutomaticFailover,omitempty"`
	// Capabilities - List of Cosmos DB capabilities for the account
	//Capabilities *[]Capability `json:"capabilities,omitempty"`
	// VirtualNetworkRules - List of Virtual Network ACL rules configured for the Cosmos DB account.
	//VirtualNetworkRules *[]VirtualNetworkRule `json:"virtualNetworkRules,omitempty"`
}
